<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Course extends Model
{
    protected $table = 'courses';
    protected $fillable = ['title', 'code', 'leader'];

    public function modules()
    {
        return $this->belongsToMany('App\Module')->withTimestamps();
    }

    public static function moduleCourseAssociations($id)
    {
        // set global variables
        $GLOBALS['course_id'] = $id;
        // create a join on multiple columns that also creates a new column 'checked'. This join can not be done in
        // eloquent as we need it to return all modules, not just those which are associated as per the above modules method.

        // first line returns all modules
        // Second part left joins to the course_module table only for the specific course
        // the select then gets all of the module data and adds an additional column' checked' making the value 1 if the association exists and 0 if it does not.
        // the get then simply runs the query.
        return DB::table('modules')
            ->leftJoin('course_module', function($join)
            {
                $join->on('course_module.module_id', '=', 'modules.id')
                    ->on('course_module.course_id', '=', DB::Raw($GLOBALS['course_id']));
            })
            ->select('modules.*', DB::Raw('IF(`course_module`.`course_id` IS NULL, 0, 1) AS `checked`'))
            ->get();
    }

}
