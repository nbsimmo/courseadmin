<?php

use Illuminate\Database\Seeder;

class CourseModuleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * Clear the table of data
         */
        DB::table('course_module')->truncate();

        $courses = [
            1,
            2,
            3,
            4
        ];

        foreach($courses as $courseID) {

            $modules = [
                1,
                2,
                3,
                4,
                5,
                6,
                7,
                8,
                9,
                10
            ];

            $moduleCount = rand(2, 11);

            /**
             * Loop though courses and create a course with random data for each course
             */
            for($i = 1; $i < $moduleCount; $i++) {

                /**
                 * Create new randomized array to shuffle users
                 */
                $moduleNumber = array_rand($modules);

                /**
                 * Add database entry for new course assigning course name, random course code and random course leader
                 */
                DB::table('course_module')->insert([
                    'course_id' => $courseID,
                    'module_id' => $modules[$moduleNumber]
                ]);

                unset($modules[$moduleNumber]);

            }

        }
    }
}
