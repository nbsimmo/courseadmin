<?php

use Illuminate\Database\Seeder;

class ItemTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        // clear the table of data.
        DB::table('items')->truncate();

        // create 40 entries in the item table with the first 5 being default items.
        for($i = 1; $i < 41; $i++){
            $default = 0;
            if($i < 6) {
                $default = 1;
            }
            DB::table('items')->insert([
                'text' => $faker->sentence($nbWords = 6),
                'default' => $default
            ]);
        };


    }
}
