@foreach($moduleCourseAssociations as $module)
    <div class="left large-2 medium-2 small-6 columns">
        {!! Form::checkbox('courseModules['.$module->id.']', $module->id, $module->checked ) !!}
        {!! Form::label('courseModules['.$module->id.']', $module->title) !!}
    </div>
@endforeach