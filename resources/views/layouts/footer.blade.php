<footer class="footer large-12 columns">
    <div class="footerpad clearfix">
        <div class="small-12 medium-9 large-9 columns bottom" id="copyright">© Copyright 2015</div>
        <div class="small-12 medium-3 large-3 columns">
            <h4>Quick Links</h4>
            <ul class="footer-links">

                @if(Auth::check())
                    <li><a href="/admin/dash">Dashboard</a></li>
                    <li><a href="/admin/course">Courses</a></li>
                    <li><a href="/admin/module">Modules</a></li>
                    <li><a href="/admin/item">Items</a></li>
                @else
                    <li><a href="/">Home</a></li>
                @endif

                @if(Auth::check())
                    <li><a href="/admin/logout">Log Out</a></li>
                @else
                    <li><a href="/admin/login">Log In</a></li>
                @endif

            </ul>
        </div>
    </div>
</footer>

