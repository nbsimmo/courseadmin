<?php
$I = new FunctionalTester($scenario);
$I->am('a CourseLeader');
$I->wantTo('see all of my courses');

// Log in as non-admin course leader
Auth::loginUsingId(12);
$I->seeAuthentication();
// Then Create a course...
$I->haveRecord('courses', [
    'id' => 5,
    'title' => 'Web Design and Development',
    'code' => 'BIS11111111',
    'leader' => 12,
]);
// When
$I->amOnPage('/admin/dash');
//  And
$I->dontSee('Add a new course');
$I->click('Courses');
// Then
$I->seeRecord('courses', ['title' => 'Web Design and Development']);
$I->amOnPage('/admin/courses');
$I->see('Courses', 'h1');
$I->see('Web Design and Development', 'a.item');
$I->dontSee('Web Systems Development', 'a.item');
// had to remove as causes issue with Web Design and Development Course
// $I->dontSee('Web', 'a.item');
$I->dontSee('Application Development', 'a.item');
// Then
$I->click('Web Design and Development');
$I->seeCurrentUrlEquals('/admin/courses/5');
$I->see('Web Design and Development', 'h1');