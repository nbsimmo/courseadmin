<?php
// added for more detailed item finding on page (for edit button a href)
use \Codeception\Util\Locator;

$I = new FunctionalTester($scenario);
$I->am('a God Admin');
$I->wantTo('update an item');

// Log in as user
// When
Auth::loginUsingId(11);
$I->seeAuthentication();

// Then Create an item.
$I->haveRecord('items', [
    'id' => '41',
    'text' => 'a test item',
    'default' => 1,
]);
$I->seeRecord('items', ['text' => 'a test item', 'id' => '41']);
// When
$I->amOnPage('/admin/items');
// Then
    // check the link is present - because there are so many edit item buttons, identify it by item id as name.
$I->seeElement('a', ['name' => '41']);
$I->click('a', ['name' => '41']);
//Then
$I->amOnPage('/admin/items/41/edit');
$I->see('Edit - a test item', 'h1');
// And
$I->amGoingTo('Clear the text field in order to submit an invalid form');
// When
$I->fillField('text', null);
$I->click('Update Item');

// the above could and probably should be expanded to run a separate test on each form filed.

// Then
$I->expectTo('See the form again with the errors');
$I->seeCurrentUrlEquals('/admin/items/41/edit');
$I->see('The text field is required');
// Then
$I->fillField('text', 'new text');
$I->checkOption('form input[name="default"]');
//  And
$I->click('Update Item');

// Then
$I->seeCurrentUrlEquals('/admin/items');
$I->seeRecord('items', ['text' => 'new text']);
$I->see('Items', 'h1');
$I->see('new text');